<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
?>
<li <?php wc_product_class( '', $product ); ?>>

<a href="<?php echo get_permalink($product->get_id()); ?>" class="product__image">
  <?php
      woocommerce_template_loop_product_thumbnail();
   ?>

</a>
<div class="product__content">
	<div class="product__content-title">
	<a href="<?php echo get_permalink($product->get_id()); ?>">
			<?php woocommerce_template_loop_product_title(); ?>
</a><?php
			the_excerpt();
			?>
	</div>
			<div class="product__content-meta">
				<?php
					woocommerce_template_single_meta();
				?>
				<?php if($product->get_attribute( 'herkunft' )) {
					?>
					<div class="product__content-meta-element">
					<span><?php _e('Herkunft', 'asia') ?>:</span>
					<span>
						<?php
							echo $product->get_attribute( 'herkunft' );
						?>
				 </span>
				</div>

					<?php
				} ?>
				
			</div>
			<?php
			if ( $product->is_type( 'simple' ) ) { ?>
				<span class="product__simple-attribute">
					<?php 
						echo $product->get_attribute( 'unit' ); 
						echo $product->get_attribute( 'einheit' ); 
						?>
				</span>
				<?php
			}
			
			// woocommerce_template_single_add_to_cart();
			woocommerce_template_loop_add_to_cart();
	 ?>


</div>
	<?php
	/**
	 * Hook: woocommerce_before_shop_loop_item.
	 *
	 * @hooked woocommerce_template_loop_product_link_open - 10
	 */
	// do_action( 'woocommerce_before_shop_loop_item' );

	/**
	 * Hook: woocommerce_before_shop_loop_item_title.
	 *
	 * @hooked woocommerce_show_product_loop_sale_flash - 10
	 * @hooked woocommerce_template_loop_product_thumbnail - 10
	 */
	// do_action( 'woocommerce_before_shop_loop_item_title' );

	/**
	 * Hook: woocommerce_shop_loop_item_title.
	 *
	 * @hooked woocommerce_template_loop_product_title - 10
	 */
	// do_action( 'woocommerce_shop_loop_item_title' );

	/**
	 * Hook: woocommerce_after_shop_loop_item_title.
	 *
	 * @hooked woocommerce_template_loop_rating - 5
	 * @hooked woocommerce_template_loop_price - 10
	 */
	// do_action( 'woocommerce_after_shop_loop_item_title' );

	/**
	 * Hook: woocommerce_after_shop_loop_item.
	 *
	 * @hooked woocommerce_template_loop_product_link_close - 5
	 * @hooked woocommerce_template_loop_add_to_cart - 10
	 */
	// do_action( 'woocommerce_after_shop_loop_item' );
	?>
</li>
