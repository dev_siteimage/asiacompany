<?php

  add_shortcode('broshure_menu', function ($atts){

    $atts = shortcode_atts( array(
        'title'      => '',
        'template'   => 'broshure-menu',

    ), $atts, 'broshure_menu' );

    ob_start();

        wp_nav_menu( [
        'theme_location'  => 'broshure',
        'container'       => 'div', 
        'container_class' => '', 
        'container_id'    => '',
        'menu_class'      => 'menu footer-menu',
        'echo'            => true,
        'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
        ] );

    return ob_get_clean();

});