(function($) {

    $(window).scroll(function() {
        if ($(window).scrollTop() >= 1) {
            $('[data-header]').addClass('site-header--fixed');
        } else {
            $('[data-header]').removeClass('site-header--fixed');
        }
    });

    $(document).on('click', '[data-menu-button]', function() {
        let th = $(this);
        let menu = $('.main-menu-0');
        $('body').toggleClass('fixed');
        th.toggleClass('active');
        menu.toggleClass('active');
    });

    $(document).on('click', '[data-shop-product-counter]', function(e) {
        e.preventDefault();
        var th = $(this);
        var closestParent = th.closest('.product__quantity');
        var addToCart = closestParent.find('.add_to_cart_button');

        var direction = th.attr('data-direction');
        var directionWrap = th.closest('.quantity');
        var quantityInput = directionWrap.find('[data-quantity-input]');
        var currentValue = +quantityInput.val();
        var newValue;

        if (direction === 'plus') {
            newValue = currentValue + 1;
            quantityInput.val(newValue);
            quantityInput.attr('value', newValue);
            addToCart.attr('data-quantity', newValue)
        } else {
            newValue = currentValue - 1 > 0 ? currentValue - 1 : 0;
            quantityInput.attr('value', newValue);
            quantityInput.val(newValue);
            addToCart.attr('data-quantity', newValue)
        }

    });
    $(document).on('click', '.woocommerce-cart-form [data-shop-product-counter]', function(e) {
        let th = $(this);
        let continter = th.closest('.woocommerce-cart-form');
        let target = continter.find('.cart-button');

        target.attr('disabled', false);

    });

    $(document).on('keypress', 'input.qty', function(e) {
        var value = $(this).attr('value');
        var closestParent = $(this).closest('.product__quantity');
        var addToCart = closestParent.find('.add_to_cart_button');
        addToCart.attr('data-quantity', value)

    });

    //Select
    $(document).ready(function() {
        $('select').niceSelect();
    });

    $(document).on('click', '.category-0 li > a', function(e) {
        e.preventDefault();
        let th = $(this);
        let url = th.attr('href');
        let container = th.closest('li');
        let ul = container.find('ul')
            // let targetUl = container.find('.sub-menu');
        let targetUl = th.next();

        if (ul.length) {
            targetUl.slideToggle();

        } else {
            window.location.replace(url);
        }

    });

    $(document).on('click', '.category-2 li > a', function(e) {
        return true
    });

    $(document).on("show_variation", '.variations_form', function(event, variation) {
        console.log(variation);
        let th = $(this);
        let container = th.closest('.product__content');
        let target = container.find('[data-add-to-cart-custom]');

        target.attr('data-product_id', variation.variation_id);
        target.attr('href', '?add-to-cart=' + variation.variation_id);
        target.attr('data-product_sku', variation.sku);
        target.removeClass('disabled');
    });

    $(window).on('load', function() {
        var inp = $('.js-woocommerce-billing-fields input');
        $('.js-woocommerce-billing-fields input').attr('readonly', true);
        $('#billing_city_field input').attr('size', '5');
        $('#billing_postcode_field input').attr('size', '5');
        $('#order_comments').attr('placeholder', 'Bspw. Lieferdatum oder abweichende Lieferadresse');
    })

    const smallDevice = window.matchMedia("(min-width: 768px)");

    smallDevice.addListener(handleDeviceChange);

    function handleDeviceChange(e) {
        if (e.matches) {
            return true

        } else {
            $(document).on('click', '.main-menu-0 li > a', function(e) {
                e.preventDefault();
                let th = $(this);
                let url = th.attr('href');
                let container = th.closest('li');
                let ul = container.find('ul')
                    // let targetUl = container.find('.sub-menu');
                let targetUl = th.next();

                if (ul.length) {
                    targetUl.slideToggle();

                } else {
                    window.location.replace(url);
                }

            });
        }
    }
    handleDeviceChange(smallDevice);

    // $(".single-product div.product .woocommerce-product-gallery img").zoom({ url: this.src });


})(jQuery);