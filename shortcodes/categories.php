<?php

add_shortcode('categories', function ($atts) {

    $atts = shortcode_atts(array(
        'title' => '',
        'template' => 'categories',

    ), $atts, 'categories');

    ob_start();

    asiacompany_render_categories_hierarchy();

    return ob_get_clean();

});

if (!function_exists('asiacompany_render_categories_hierarchy')) {
    function asiacompany_render_categories_hierarchy($parentId = 0, $level = 0) {
        $args = [
            'taxonomy' => 'product_cat',
            'orderby' => 'title',
            'order' => 'ASC',
            'hide_empty' => true,
            'parent' => $parentId
        ];
        $categories = get_terms($args);
        $class = $class = 'category-' . $level;
        if (!empty($categories)) {
            echo '<ul class="'. $class .'">';
            foreach ($categories as $category) {
                echo '<li><a href="' . get_term_link($category) . '">' . $category->name . '</a>';
                asiacompany_render_categories_hierarchy($category->term_id, $level+1);
                echo '</li>';
            }
            echo '</ul>';
        }
    }
}