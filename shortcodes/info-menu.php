<?php

  add_shortcode('info_menu', function ($atts){

    $atts = shortcode_atts( array(
        'title'      => '',
        'template'   => 'info-menu',

    ), $atts, 'info_menu' );

    ob_start();

        wp_nav_menu( [
        'theme_location'  => 'info',
        'container'       => 'div', 
        'container_class' => '', 
        'container_id'    => '',
        'menu_class'      => 'menu footer-menu',
        'echo'            => true,
        'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
        ] );

    return ob_get_clean();

});