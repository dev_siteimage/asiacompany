<?php $fields = get_field('categories'); ?>
<ul class="category">

  <?php foreach($fields as $field) : 

  if ($field['image']) : ?>

  <li class="category__item">

    <a class="category__link" href="<?php echo $field['link']; ?>">
      <img class="category__img" src="<?php echo $field['image']; ?>" alt="<?php echo $field['title']; ?>">
      <span class="category__title"><?php echo $field['title']; ?></span>
    </a>

  </li>

  <?php endif; endforeach; ?>

</ul>
