<?php

  add_shortcode('contacts', function ($atts){

    $atts = shortcode_atts( array(
        'title'      => '',
        'template'   => 'contacts',

    ), $atts, 'contacts' );

    ob_start();

    ?>

    <ul class="contacts">
        <li class="contacts__item">
            <span class="contacts__item-text"><?php echo get_field('contact-title'); ?></span>
        </li>
        <li class="contacts__item">
            <span class="contacts__item-text"><?php echo get_field('contact-address'); ?></span>
        </li>
        <li class="contacts__item">
            <span class="contacts__item-text"><?php echo get_field('contact-address2'); ?></span>
        </li>
        <li class="contacts__item">
            <a href="tel:<?php echo get_field('contact-phone'); ?>" class="contacts__item-text">Tel: <?php echo get_field('contact-phone'); ?></a>
        </li>
        <li class="contacts__item">
            <a href="tel:<?php echo get_field('contact-fax'); ?>" class="contacts__item-text">Fax: <?php echo get_field('contact-fax'); ?></a>
        </li>
        <li class="contacts__item">
            <a href="mailto:<?php echo get_field('contact-email'); ?>" class="contacts__item-text"><?php echo get_field('contact-email'); ?></a>
        </li>
        <li class="contacts__item">
            <span class="contacts__item-text"><?php echo get_field('contact-work-time'); ?></span>
        </li>
    </ul>

    <?php
    return ob_get_clean();

});