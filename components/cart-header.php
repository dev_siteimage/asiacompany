<?php global $woocommerce; ?>
<a class="top-menu__link" href="<?php echo wc_get_cart_url();; ?>"
title="<?= _e('Warenkorb', 'asiacompany') ?>">
  <span class="top-menu__link-text">
    <?= _e('Warenkorb', 'asiacompany') ?>
  </span>
  <span class="top-menu__link-icon">
    <?php echo asiacompany_svg('shopping-basket'); ?>
  </span>
  <span class="top-menu__cart-number2">
    <?php echo count(WC()->cart->get_cart())?>
  </span>
</a>