<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package storefront
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2.0">
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
  <title>TAC - The Asia Company S.A. - Asiatische Lebensmittel und Gastrobedarf - Home</title>
  <meta property="og:site_name" content="TAC - The Asia Company S.A. - Asiatische Lebensmittel und Gastrobedarf" />
  <meta property="og:title" content="TAC - The Asia Company S.A. - Asiatische Lebensmittel und Gastrobedarf" />
  <meta property="og:description"
    content="Ihr Partner für asiatische Spezialitäten in der Schweiz​ - The Asia Company S.A." />
  <meta property="og:image" content="http://www.theasiacompany.ch/uploads/9/1/5/3/91534146/tac-logo.png" />
  <meta property="og:url" content="http://www.theasiacompany.ch/" />
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;700&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@300;400;500;700&display=swap" rel="stylesheet">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Orienta&display=swap" rel="stylesheet">

  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

  <?php wp_body_open(); ?>

  <?php do_action( 'storefront_before_site' ); ?>

  <div id="page" class="hfeed site">
    <?php do_action( 'storefront_before_header' ); ?>

    <header id="masthead" class="site-header" role="banner" style="<?php storefront_header_styles(); ?>" data-header>

      <div class="site-header__top">

        <div class="site-header__logo">
          <?php echo get_custom_logo(); ?>
          <img class="white-logo" src="http://dev.theasiacompany.imagecmsdemo.net/wp-content/uploads/2021/03/logo-white.png" alt="TAC - The Asia Company">
        </div>

        <div class="site-header__search">
          <?php get_product_search_form(); ?>
        </div>

        <ul class="site-header__top-menu top-menu">
          <li class="top-menu__item">
            <a href="<?= get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" class="top-menu__link">
              <span class="top-menu__link-icon">
                <?php echo asiacompany_svg('exit'); ?>
              </span>
              <span class="top-menu__link-text">
                <?php 
          if(is_user_logged_in()){
            _e('Konto', 'asiacompany'); 
          } else{
            _e('Anmelden', 'asiacompany'); 
          }
            ?>
              </span>
            </a>
          </li>
          <li class="top-menu__item">
            <?php get_template_part( 'components/cart-header' ); ?>
          </li>
        </ul>

        <div class="hamburger" data-menu-button>
          <span></span>
        </div>

      </div>

      <div class="site-header__menu">
        <div class="site-header__main-menu js-main-menu">
          <?php 
    
      if (!function_exists('asiacompany_render_categories_hierarchy_menu')) {
        function asiacompany_render_categories_hierarchy_menu($parentId = 0, $level = 0) {
            $args = [
                'taxonomy' => 'product_cat',
                'hide_empty' => true,
                'parent' => $parentId
            ];
            $categories = get_terms($args);
            $class = $class = 'main-menu-' . $level;
            if (!empty($categories)) {
                echo '<ul class="'. $class .'">';
                foreach ($categories as $category) {
                    echo '<li><a href="' . get_term_link($category) . '">' . $category->name . '</a>';
                    asiacompany_render_categories_hierarchy_menu($category->term_id, $level+1);
                    echo '</li>';
                }
                echo '</ul>';
            }
        }
    }
    asiacompany_render_categories_hierarchy_menu();
    ?>
        </div>
      </div>


    </header><!-- #masthead -->

    <?php
	/**
	 * Functions hooked in to storefront_before_content
	 *
	 * @hooked storefront_header_widget_region - 10
	 * @hooked woocommerce_breadcrumb - 10
	 */
	do_action( 'storefront_before_content' );
	?>

    <div id="content" class="site-content" tabindex="-1">
      <?php get_template_part( 'components/main-banner' ); ?>
      <div class="col-full">

        <?php
		do_action( 'storefront_content_top' );