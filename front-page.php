<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package storefront
 */

get_header(); ?>

	<div id="primary" class="content-area main-page">
		<main id="main" class="site-main" role="main">
    <?php the_content(); ?>
    <?php get_template_part( 'components/main-text' ); ?>

    <?php get_template_part( 'components/categories' ); ?>

    </main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer();
