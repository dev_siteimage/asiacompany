/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/js/app.js":
/*!***********************!*\
  !*** ./src/js/app.js ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

(function ($) {
  $(window).scroll(function () {
    if ($(window).scrollTop() >= 1) {
      $('[data-header]').addClass('site-header--fixed');
    } else {
      $('[data-header]').removeClass('site-header--fixed');
    }
  });
  $(document).on('click', '[data-menu-button]', function () {
    var th = $(this);
    var menu = $('.main-menu-0');
    $('body').toggleClass('fixed');
    th.toggleClass('active');
    menu.toggleClass('active');
  });
  $(document).on('click', '[data-shop-product-counter]', function (e) {
    e.preventDefault();
    var th = $(this);
    var closestParent = th.closest('.product__quantity');
    var addToCart = closestParent.find('.add_to_cart_button');
    var direction = th.attr('data-direction');
    var directionWrap = th.closest('.quantity');
    var quantityInput = directionWrap.find('[data-quantity-input]');
    var currentValue = +quantityInput.val();
    var newValue;

    if (direction === 'plus') {
      newValue = currentValue + 1;
      quantityInput.val(newValue);
      quantityInput.attr('value', newValue);
      addToCart.attr('data-quantity', newValue);
    } else {
      newValue = currentValue - 1 > 0 ? currentValue - 1 : 0;
      quantityInput.attr('value', newValue);
      quantityInput.val(newValue);
      addToCart.attr('data-quantity', newValue);
    }
  });
  $(document).on('click', '.woocommerce-cart-form [data-shop-product-counter]', function (e) {
    var th = $(this);
    var continter = th.closest('.woocommerce-cart-form');
    var target = continter.find('.cart-button');
    target.attr('disabled', false);
  });
  $(document).on('keypress', 'input.qty', function (e) {
    var value = $(this).attr('value');
    var closestParent = $(this).closest('.product__quantity');
    var addToCart = closestParent.find('.add_to_cart_button');
    addToCart.attr('data-quantity', value);
  }); //Select

  $(document).ready(function () {
    $('select').niceSelect();
  });
  $(document).on('click', '.category-0 li > a', function (e) {
    e.preventDefault();
    var th = $(this);
    var url = th.attr('href');
    var container = th.closest('li');
    var ul = container.find('ul'); // let targetUl = container.find('.sub-menu');

    var targetUl = th.next();

    if (ul.length) {
      targetUl.slideToggle();
    } else {
      window.location.replace(url);
    }
  });
  $(document).on('click', '.category-2 li > a', function (e) {
    return true;
  });
  $(document).on("show_variation", '.variations_form', function (event, variation) {
    console.log(variation);
    var th = $(this);
    var container = th.closest('.product__content');
    var target = container.find('[data-add-to-cart-custom]');
    target.attr('data-product_id', variation.variation_id);
    target.attr('href', '?add-to-cart=' + variation.variation_id);
    target.attr('data-product_sku', variation.sku);
    target.removeClass('disabled');
  });
  $(window).on('load', function () {
    var inp = $('.js-woocommerce-billing-fields input');
    $('.js-woocommerce-billing-fields input').attr('readonly', true);
    $('#billing_city_field input').attr('size', '5');
    $('#billing_postcode_field input').attr('size', '5');
    $('#order_comments').attr('placeholder', 'Bspw. Lieferdatum oder abweichende Lieferadresse');
  });
  var smallDevice = window.matchMedia("(min-width: 768px)");
  smallDevice.addListener(handleDeviceChange);

  function handleDeviceChange(e) {
    if (e.matches) {
      return true;
    } else {
      $(document).on('click', '.main-menu-0 li > a', function (e) {
        e.preventDefault();
        var th = $(this);
        var url = th.attr('href');
        var container = th.closest('li');
        var ul = container.find('ul'); // let targetUl = container.find('.sub-menu');

        var targetUl = th.next();

        if (ul.length) {
          targetUl.slideToggle();
        } else {
          window.location.replace(url);
        }
      });
    }
  }

  handleDeviceChange(smallDevice); // $(".single-product div.product .woocommerce-product-gallery img").zoom({ url: this.src });
})(jQuery);

/***/ }),

/***/ "./src/scss/app.scss":
/*!***************************!*\
  !*** ./src/scss/app.scss ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 0:
/*!*************************************************!*\
  !*** multi ./src/js/app.js ./src/scss/app.scss ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! D:\Work\ProgramsInstalations\OpenServer\domains\dev.asiacompany\wp-content\themes\asiacompany\src\js\app.js */"./src/js/app.js");
module.exports = __webpack_require__(/*! D:\Work\ProgramsInstalations\OpenServer\domains\dev.asiacompany\wp-content\themes\asiacompany\src\scss\app.scss */"./src/scss/app.scss");


/***/ })

/******/ });