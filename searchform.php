<form class="search-form" autocomplete="off" data-search role="search" method="get" id="searchform" action="<?php echo home_url( '/' ) ?>" >
  <input class="search-form__input" data-search-input  type="text" placeholder="Suchen" value="<?php echo get_search_query() ?>" name="s" id="s" />
  <span class="search-form__decoration"></span>
	<button class="search-form__button" data-search-button-submit type="submit"><?php echo asiacompany_svg('search'); ?></button>
</form>