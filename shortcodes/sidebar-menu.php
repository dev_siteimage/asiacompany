<?php

  add_shortcode('sidebar_menu', function ($atts){

    $atts = shortcode_atts( array(
        'title'      => '',
        'template'   => 'sidebar-menu',

    ), $atts, 'sidebar_menu' );

    ob_start();

    wp_nav_menu( [
      'theme_location'  => 'primary',
      'menu'            => '', 
      'container'       => 'div', 
      'container_class' => 'sidebar-menu-container',
      'menu_class'      => 'menu sidebar-menu js-sidebar-menu', 
      'fallback_cb'     => 'wp_page_menu',
      'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
    ] );

    return ob_get_clean();

});