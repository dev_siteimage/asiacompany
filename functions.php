<?php 

add_action(
  "wp_enqueue_scripts",
  function () {
      wp_enqueue_style(
        'asiacompany-select-style',
        get_stylesheet_directory_uri() . '/public/css/libs/nice-select.css',
        []
      );
      wp_enqueue_style(
          'asiacompany-style',
          get_stylesheet_directory_uri() . '/public/css/app.css',
          []
      );
      wp_enqueue_script(
        "asiacompany-select-script",
        get_stylesheet_directory_uri() . "/public/js/libs/jquery.nice-select.js",
        array()
      );
      wp_enqueue_script(
        "asiacompany-zoom-script",
        get_stylesheet_directory_uri() . "/public/js/libs/jquery.zoom.min.js",
        array()
      );
      wp_enqueue_script(
          "asiacompany-script",
          get_stylesheet_directory_uri() . "/public/js/app.js",
          array()
      );
  },
  99
);

//SVG

function asiacompany_svg($file){
  return file_get_contents(__DIR__.'/icons/'.$file.'.svg');
}


require dirname( __FILE__ ) . '/shortcodes/init.php';

add_action( 'after_setup_theme', function(){
	register_nav_menus( [
		'primary'   => __( 'Primary Menu', 'asiacompany' ),
    'info' => __( 'Info Menu', 'asiacompany' ),
    'broshure' => __( 'Broshure Menu', 'asiacompany' ),
    'handheld'  => __( 'Handheld Menu', 'asiacompany' ),
    'footer'  => __( 'Footer Menu', 'asiacompany' ),
	] );
} );


add_action('init','all_my_hooks');
function all_my_hooks(){
    remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
    remove_action( 'woocommerce_before_shop_loop', 'storefront_woocommerce_pagination', 30 );
    remove_action( 'woocommerce_after_shop_loop', 'woocommerce_catalog_ordering', 10 );
    remove_action( 'woocommerce_after_shop_loop', 'woocommerce_result_count', 20 );
}

add_filter( 'woocommerce_checkout_fields', 'asiacompany_billing_order' );
 
function asiacompany_billing_order( $checkout_fields ) {
  $checkout_fields['billing']['billing_company']['priority'] = 4;
  $checkout_fields['billing']['billing_first_name']['priority'] = 10;
  $checkout_fields['billing']['billing_postcode']['priority'] = 60;

  $checkout_fields['billing']['billing_company']['required'] = false;
  $checkout_fields['billing']['billing_postcode']['required'] = false;
  $checkout_fields['billing']['billing_address_1']['required'] = false;
  $checkout_fields['billing']['billing_city']['required'] = false;
  $checkout_fields['billing']['billing_phone']['required'] = false;
  
  unset($checkout_fields['billing']['billing_country']);
  // unset($checkout_fields['billing']['billing_city']);
  unset($checkout_fields['billing']['billing_state']);
  // unset($checkout_fields['billing']['billing_postcode']);
  unset($checkout_fields['billing']['billing_address_2']);
  unset($checkout_fields['billing']['billing_last_name']);


	return $checkout_fields;
}

add_filter('woocommerce_checkout_fields','asiacompany_fields_no_label');

function asiacompany_fields_no_label($fields) {
    foreach ($fields as $category => $value) {
        foreach ($fields[$category] as $field => $property) {
            unset($fields[$category][$field]['label']);
        }
    }
     return $fields;
}


add_action( 'get_header', 'asiacompany_remove_storefront_sidebar' );
 
function asiacompany_remove_storefront_sidebar() {
 
     if ( is_front_page() && is_home() || is_checkout() || is_account_page() || is_cart() ) {
 
       remove_action( 'storefront_sidebar', 'storefront_get_sidebar', 10 );
 
    }
 
}

function wooc_extra_register_fields() {?>
<p class="register__text">
  <?php _e('Wenn Sie von unserem Online Shop bestellen möchten, registrieren Sie sich bitte mit dem untenstehenden Formular und wir setzen uns mit Ihnen in Verbindung. Die Bestellung kann erst abgesendet werden, nachdem Ihre Registrierung von uns bestätigt wurde', 'asia') ?>
</p>
<p class="form-row form-row-wide">
  <label for="reg_billing_company"><?php _e( 'Firma', 'woocommerce' ); ?><span class="required">*</span></label>
  <input type="text" class="input-text" name="billing_company" id="reg_billing_company"
    value="<?php if ( ! empty( $_POST['billing_company'] ) ) esc_attr_e( $_POST['billing_company'] ); ?>" />
</p>
<p class="form-row form-row-wide">
  <label for="_customer_number"><?php _e('Kundennummer (falls vorhanden)', 'theasiacompany-customization'); ?></label>
  <input type="text" class="input-text" name="_customer_number" id="_customer_number"
    value="<?php if ( ! empty( $_POST['_customer_number'] ) ) esc_attr_e( $_POST['_customer_number'] ); ?>" />
</p>
<p class="form-row form-row-first">
  <label for="reg_billing_first_name"><?php _e( 'Ansprechpartner ', 'woocommerce' ); ?></label>
  <input type="text" class="input-text" name="billing_first_name" id="reg_billing_first_name"
    value="<?php if ( ! empty( $_POST['billing_first_name'] ) ) esc_attr_e( $_POST['billing_first_name'] ); ?>" />
</p>
<p class="form-row form-row-wide">
  <label for="billing_address_1"><?php _e( 'Strasse und Nr.', 'woocommerce' ); ?><span class="required">*</span></label>
  <input type="text" class="input-text" name="billing_address_1" id="reg_billing_address_1"
    value="<?php if ( ! empty( $_POST['billing_address_1'] ) ) esc_attr_e( $_POST['billing_address_1'] ); ?>" />
</p>
<div class="register__info">
  <p class="form-row form-row-wide">
    <label for="billing_postcode"><?php _e( 'PLZ', 'woocommerce' ); ?><span class="required">*</span></label>
    <input type="text" class="input-text" name="billing_postcode" id="reg_billing_postcode"
      value="<?php if ( ! empty( $_POST['billing_postcode'] ) ) esc_attr_e( $_POST['billing_postcode'] ); ?>" />
  </p>
  <p class="form-row form-row-wide">
    <label for="billing_city"><?php _e( 'Ort', 'woocommerce' ); ?><span class="required">*</span></label>
    <input type="text" class="input-text" name="billing_city" id="reg_billing_city"
      value="<?php if ( ! empty( $_POST['billing_city'] ) ) esc_attr_e( $_POST['billing_city'] ); ?>" />
  </p>
</div>

<p class="form-row form-row-wide">
  <label for="billing_country"><?php _e( 'Land', 'woocommerce' ); ?></label>
  <input type="text" class="input-text input-text--read" readonly name="billing_country" id="reg_billing_country"
    value="<?php _e('Switzerland', 'woocommerce') ?>" />
</p>
<p class="form-row form-row-wide">
  <label for="reg_billing_phone"><?php _e( 'Telefon', 'woocommerce' ); ?><span class="required">*</span></label>
  <input type="tel" class="input-text" name="billing_phone" id="reg_billing_phone"
    value="<?php if ( ! empty( $_POST['billing_phone'] ) ) esc_attr_e( $_POST['billing_phone'] ); ?>" />
</p>

<?php
}
add_action( 'woocommerce_register_form_start', 'wooc_extra_register_fields' );

function new_submenu_class($menu) {    
  $menu = preg_replace('/ class="sub-menu"/','/ class="sub-menu js-sub-menu" /',$menu);        
  return $menu;      
}

add_filter('wp_nav_menu','new_submenu_class'); 
if (!function_exists('asiacompany_woocommerce_placeholder_img_src')) {
    function asiacompany_woocommerce_placeholder_img_src($src) {
        $placeholderPath = dirname(__FILE__) . '/woocommerce_placeholder_image.png';
        $dirBaseName = pathinfo(dirname(__FILE__), PATHINFO_BASENAME);
        if (is_file($placeholderPath)) {
            $src = home_url('/wp-content/themes/' . $dirBaseName . '/woocommerce_placeholder_image.png');
        }
        return $src;
    }
}

add_filter('woocommerce_placeholder_img_src', 'asiacompany_woocommerce_placeholder_img_src');


add_action( 'woocommerce_register_form', 'asiacompany_registration_privacy_policy', 11 );
   
function asiacompany_registration_privacy_policy() {
  
woocommerce_form_field( 'privacy_policy_reg', array(
    'type'          => 'checkbox',
    'class'         => array('form-row privacy'),
    'label_class'   => array('woocommerce-form__label woocommerce-form__label-for-checkbox checkbox'),
    'input_class'   => array('woocommerce-form__input woocommerce-form__input-checkbox input-checkbox'),
    'required'      => true,
    'label'         => 'Ich bin mit den <a target="_blank" href="http://dev.theasiacompany.imagecmsdemo.net/wp-content/uploads/2020/12/AGB.pdf">AGB</a> und der <a target="_blank" href="http://dev.theasiacompany.imagecmsdemo.net/wp-content/uploads/2020/12/Tourenplan.pdf">Datenschutzerklärung</a> einverstanden',
)); 
  
}
  
// Show error if the user does not tick
   
add_filter( 'woocommerce_registration_errors', 'asiacompany_validate_privacy_registration', 10, 3 );
  
function asiacompany_validate_privacy_registration( $errors, $username, $email ) {
    if ( ! (int) isset( $_POST['privacy_policy_reg'] ) ) {
        $errors->add( 'privacy_policy_reg_error', __( 'Privacy Policy consent is required!', 'woocommerce' ) );
    }
return $errors;
}


add_filter( 'woocommerce_add_to_cart_fragments', 'asiacompany_cart_count_fragments', 10, 1 );

function asiacompany_cart_count_fragments( $fragments ) {

    $fragments['span.top-menu__cart-number2'] = '<span class="top-menu__cart-number2">' . count(WC()->cart->get_cart()) . '</span>';

    return $fragments;


}

add_action('wp_head','add_to_cart_script');
function add_to_cart_script(){
  if(is_archive()){
    wp_enqueue_script('wc-add-to-cart-variation');
  }
}

add_filter( 'woocommerce_get_item_data', 'wc_checkout_description_so_15127954', 10, 2 );
function wc_checkout_description_so_15127954( $other_data, $cart_item )
{
    $post_data = get_post( $cart_item['product_id'] );
    $other_data[] = array( 'name' =>  $post_data->post_excerpt );
    return $other_data;
}


add_filter( 'woocommerce_account_menu_items', 'custom_remove_downloads_my_account', 999 );
 
function custom_remove_downloads_my_account( $items ) {
unset($items['downloads']);
unset($items['edit-address']);
return $items;
}

function wpse71032_change_tml_registration_message( $translated_text, $text, $domain ) {
  if( $domain === 'theme-my-login' &&
      $text === 'Your registration was successful but you must now confirm your email address before you can log in. Please check your email and click on the link provided.'
  ) {
      /* in the below, change the first argument to the message you want
         and the second argument to your theme's textdomain */
      $translated_text = __( '%Registration Message%', 'theme_text_domain' );
  }
  return $translated_text;
}
add_filter( 'gettext', 'wpse71032_change_tml_registration_message', 20, 3 );

add_filter( 'woocommerce_dropdown_variation_attribute_options_html', 'filter_dropdown_option_html', 12, 2 );
function filter_dropdown_option_html( $html, $args ) {
    $show_option_none_text = $args['show_option_none'] ? $args['show_option_none'] : __( 'Choose an option', 'woocommerce' );
    $show_option_none_html = '<option value="">' . esc_html( $show_option_none_text ) . '</option>';

    $html = str_replace($show_option_none_html, '', $html);

    return $html;
}

function asia_new_orders_columns( $columns = array() ) {

  if( isset($columns['order-total']) ) {
      unset( $columns['order-number'] );
      unset( $columns['order-status'] );
      unset( $columns['order-total'] );
      unset( $columns['order-actions'] );
  }

  $columns['order-total'] = __( 'Anzahl Artikel', 'Text Domain' );
  $columns['order-actions'] = __( 'Aktionen', 'Text Domain' );

  return $columns;
}
add_filter( 'woocommerce_account_orders_columns', 'asia_new_orders_columns' );

// Product image zoom
add_filter( 'woocommerce_single_product_zoom_options', 'asia_single_product_zoom_options' );
function asia_single_product_zoom_options( $zoom_options ) {
    $zoom_options['magnify'] = 0.5;
    return $zoom_options;
}

// function remove_image_zoom_support() {
//   remove_theme_support( 'wc-product-gallery-zoom' );
// }
// add_action( 'after_setup_theme', 'remove_image_zoom_support', 100 );